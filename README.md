# MongoConnector

## Usage

Peerdependency: @krknet/profiler

```javascript
const Connection = require('@krknet/mongo-connector')
global.__db = new Connection({ url: global.__config.mongo, initiatiors: [] })

await global.__db.connect()
await global.__db.disconnect()
```

## Options

```javascript
{
  initiatiors: [],
  doLog: true,
  url: '',
  castIds: false
}
```

## Functions

```javascript
async connect ()

async disconnect ()

get ()
```
