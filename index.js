const monk = require('monk')
const Profiler = require('@krknet/profiler')

module.exports = class Connection {
  constructor (options = {}) {
    this.options = options

    this.options.url = this.options.url || ''
    if (this.options.doLog === undefined) this.options.doLog = true
    if (this.options.castIds === undefined) this.options.castIds = false
    this.options.initiatiors = this.options.initiatiors || []

    this.con = null
  }

  connect () {
    const profiler = new Profiler('MongoDB-Connection')
    if (this.options.doLog) profiler.start()

    return monk(this.options.url, { collectionOptions: { castIds: false } })
      .then(async db => {
        this.con = db
        for (const initiatior of this.options.initiatiors) await initiatior(this)
        if (this.options.doLog) profiler.succeed()
      })
      .catch(err => {
        this.con = null
        if (this.options.doLog) profiler.fail()
        throw new Error(err)
      })
  }

  disconnect () {
    if (!this.con) return

    return this.con.close()
      .then(() => {
        this.con = null
        if (this.options.doLog) Profiler.success('MongoDB-Connection closed')
      })
  }

  get (name, options = {}) {
    return this.con.get(name, options)
  }
}
